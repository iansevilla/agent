#import <Foundation/Foundation.h>

@interface NativeAlert : NSObject

- (void)createAlert: (const char*) title withMsg: (const char*) msg withCancelButtonTitle: (const char*) cancelButtonTitle withOtherButtonTitle: (const char*) otherButtonTitle;

- (void)createAlert: (const char*) title withMsg: (const char*) msg withCancelButtonTitle: (const char*) cancelButtonTitle;


@end

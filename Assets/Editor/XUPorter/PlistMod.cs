using UnityEngine;
using System.IO;
using System.Xml;

	public class PlistMod
	{
		private static XmlNode FindPlistDictNode(XmlDocument doc)
        {
            var curr = doc.FirstChild;
            while(curr != null)
            {
                if(curr.Name.Equals("plist") && curr.ChildNodes.Count == 1)
                {
                    var dict = curr.FirstChild;
                    if(dict.Name.Equals("dict"))
                        return dict;
                }
                curr = curr.NextSibling;
            }
            return null;
        }
        
        private static XmlElement AddChildElement(XmlDocument doc, XmlNode parent, string elementName, string innerText=null)
        {
            var newElement = doc.CreateElement(elementName);
            if(!string.IsNullOrEmpty(innerText))
                newElement.InnerText = innerText;
            
            parent.AppendChild(newElement);
            return newElement;
        }

        private static bool HasKey(XmlNode dict, string keyName)
        {
            var curr = dict.FirstChild;
            while(curr != null)
            {
                if(curr.Name.Equals("key") && curr.InnerText.Equals(keyName))
                    return true;
                curr = curr.NextSibling;
            }
            return false;
        }

		private static XmlNode GetKey(XmlNode dict, string keyName)
		{
			var curr = dict.FirstChild;
			while(curr != null)
			{
				if(curr.Name.Equals("key") && curr.InnerText.Equals(keyName))
					return curr;
				curr = curr.NextSibling;
			}
			return null;
		}

        public static void UpdatePlist(string path)
        {
            const string fileName = "Info.plist";
            string fullPath = Path.Combine(path, fileName);
            
            var doc = new XmlDocument();
            doc.Load(fullPath);

            var dict = FindPlistDictNode(doc);
            if(dict == null)
            {
                Debug.LogError("Error parsing " + fullPath);
                return;
            }
            
            //add the app id to the plist
            //the xml should end up looking like this
            /*
            <key>UIViewControllerBasedStatusBarAppearance</key>
            <false/>
             */
			if(!HasKey(dict, "UIViewControllerBasedStatusBarAppearance"))
            {
				AddChildElement(doc, dict, "key", "UIViewControllerBasedStatusBarAppearance");
                AddChildElement(doc, dict, "false");
            }
            
            doc.Save(fullPath);
            
            //the xml writer barfs writing out part of the plist header.
            //so we replace the part that it wrote incorrectly here
            var reader = new StreamReader(fullPath);
            string textPlist = reader.ReadToEnd();
            reader.Close();
            
            int fixupStart = textPlist.IndexOf("<!DOCTYPE plist PUBLIC", System.StringComparison.Ordinal);
            if(fixupStart <= 0)
                return;
            int fixupEnd = textPlist.IndexOf('>', fixupStart);
            if(fixupEnd <= 0)
                return;
            
            string fixedPlist = textPlist.Substring(0, fixupStart);
            fixedPlist += "<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">";
            fixedPlist += textPlist.Substring(fixupEnd+1);
            
            var writer = new StreamWriter(fullPath, false);
            writer.Write(fixedPlist);
            writer.Close();
        }
    }
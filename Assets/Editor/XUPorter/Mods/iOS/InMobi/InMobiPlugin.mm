#import "InMobiPlugin.h"

@implementation InMobiPlugin

- (id)init
{
    self = [super init];
    
    return self;
}

-(void) initInMobi: (const char*) adUnitID
{
    NSLog(@"InMobiPlugin :: initInMobi");
    
    [InMobi initialize: [NSString stringWithUTF8String: adUnitID]];
    [InMobi setLogLevel:IMLogLevelDebug];
}

-(void) createBannerAd: (const char*) adUnitID
{
    NSLog(@"InMobiPlugin :: createBannerAd");
    
    [self removeBannerAd];
    
    int slotSize = [self getOptimalAdSize];
    
    self.banner = [[IMBanner alloc] initWithFrame:CGRectMake(0, 0, [self getWidthForSlotSize: slotSize], [self getHeightForSlotSize: slotSize])
                                            appId: [NSString stringWithUTF8String: adUnitID]
                                           adSize: slotSize];
    
    self.banner.refreshInterval = REFRESH_INTERVAL_OFF;
    
    self.banner.delegate = self;
    
    self.banner.center = CGPointMake(UnityGetGLViewController().view.center.x, self.banner.center.y);
    
    [self.banner loadBanner];
}

-(void) createBottomBannerAd: (const char*) adUnitID
{
    NSLog(@"InMobiPlugin :: createBottomBannerAd");
    
    [self removeBannerAd];
    
    int slotSize = [self getOptimalAdSize];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    
    self.banner = [[IMBanner alloc] initWithFrame:CGRectMake(0, screenHeight-[self getHeightForSlotSize: slotSize], [self getWidthForSlotSize: slotSize], [self getHeightForSlotSize: slotSize])
                                            appId: [NSString stringWithUTF8String: adUnitID]
                                           adSize: slotSize];
    
    self.banner.refreshInterval = REFRESH_INTERVAL_OFF;
    
    self.banner.delegate = self;
    
    self.banner.center = CGPointMake(UnityGetGLViewController().view.center.x, self.banner.center.y);
    
    [self.banner loadBanner];
}


-(void) removeBannerAd
{
    NSLog(@"InMobiPlugin :: removeBannerAd");
    
    if(self.banner != nil)
    {   [self.banner removeFromSuperview];
        self.banner.delegate = nil;
        self.banner = nil;
    }
}

-(int) getOptimalAdSize
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    if(screenWidth >= 728)
    {   return IM_UNIT_728x90;
    }   else
    {   return IM_UNIT_320x50;
    }
}

-(int) getWidthForSlotSize: (int) slotSize
{
    if(slotSize == 11)
    {	return 728;
    }	else
    {	return 320;
    }
}

-(int) getHeightForSlotSize: (int) slotSize
{
    if(slotSize == 11)
    {	return 90;
    }	else
    {	return 50;
    }
}

- (void)bannerDidReceiveAd:(IMBanner *)banner
{   NSLog(@"InMobiPlugin :: bannerDidReceiveAd");
    [UnityGetGLViewController().view addSubview: banner];
    
    UnitySendMessage("~OInMobiHandler", "bannerDidReceiveAd", "");
}

- (void)banner:(IMBanner *)banner didFailToReceiveAdWithError:(IMError *)error
{   NSString *errorMessage = [NSString stringWithFormat:@"Error code: %d, message: %@", [error code], [error localizedDescription]];
    NSLog(@"InMobiPlugin :: bannerDidFailToReceiveAdWithError %@", errorMessage);
    
    UnitySendMessage("~OInMobiHandler", "bannerDidFailToReceiveAd", "");
}

-(void)bannerDidInteract:(IMBanner *)banner withParams:(NSDictionary *)dictionary
{   NSLog(@"InMobiPlugin :: bannerDidInteract with params: %@", [dictionary description]);
    
    UnitySendMessage("~OInMobiHandler", "bannerDidInteract", "");
}

- (void)bannerWillPresentScreen:(IMBanner *)banner
{   NSLog(@"InMobiPlugin :: bannerWillPresentScreen");
    
    UnitySendMessage("~OInMobiHandler", "bannerWillPresentScreen", "");
}

- (void)bannerWillDismissScreen:(IMBanner *)banner
{   NSLog(@"InMobiPlugin :: bannerWillDismissScreen");
    
    UnitySendMessage("~OInMobiHandler", "bannerWillDismissScreen", "");
}

- (void)bannerDidDismissScreen:(IMBanner *)banner
{   NSLog(@"InMobiPlugin :: bannerDidDismissScreen");
    
    UnitySendMessage("~OInMobiHandler", "bannerDidDismissScreen", "");
}

- (void)bannerWillLeaveApplication:(IMBanner *)banner
{   NSLog(@"InMobiPlugin :: bannerWillLeaveApplication");
    
    UnitySendMessage("~OInMobiHandler", "bannerWillLeaveApplication", "");
}

-(void) createInterstitialAd: (const char*) adUnitID
{
    NSLog(@"InMobiPlugin :: createInterstitialAd");
    
    if(self.adInterstitial != nil)
    {   self.adInterstitial.delegate = nil;
        self.adInterstitial = nil;
    }
    
    self.adInterstitial = [[IMInterstitial alloc] initWithAppId: [NSString stringWithUTF8String: adUnitID]];
    self.adInterstitial.delegate = self;
    self.adInterstitial.additionaParameters = @{ @"tp": @"p_unity" };
	
    [self.adInterstitial loadInterstitial];
}

-(bool) isInterstitialAdAvailable
{
    NSLog(@"InMobiPlugin :: createInterstitialAd");
    
    if (self.adInterstitial && self.adInterstitial.state == kIMInterstitialStateReady)
    {   return YES;
    }   else
    {   return NO;
    }
}

-(void) showInterstitialAd
{
    NSLog(@"InMobiPlugin :: showInterstitialAd");
    
    if (self.adInterstitial && self.adInterstitial.state == kIMInterstitialStateReady)
    {   [self.adInterstitial presentInterstitialAnimated:YES];
    }
}

- (void)interstitialDidReceiveAd:(IMInterstitial *)ad
{   NSLog(@"InMobiPlugin :: interstitialDidReceiveAd");
    
    UnitySendMessage("~OInMobiHandler", "interstitialDidReceiveAd", "");
}

- (void)interstitial:(IMInterstitial *)ad didFailToReceiveAdWithError:(IMError *)error
{   NSString *errorMessage = [NSString stringWithFormat:@"Error code: %d, message: %@", [error code], [error localizedDescription]];
    
    NSLog(@"InMobiPlugin :: interstitialDidFailToReceiveAdWithError %@", errorMessage);
    
    UnitySendMessage("~OInMobiHandler", "interstitialDidFailToReceiveAd", "");
}

-(void)interstitialDidInteract:(IMInterstitial *)ad withParams:(NSDictionary *)dictionary
{
    NSLog(@"InMobiPlugin :: interstitialDidInteract with params %@", [dictionary description]);
    
    UnitySendMessage("~OInMobiHandler", "interstitialDidInteract", "");
}

- (void)interstitialWillPresentScreen:(IMInterstitial *)ad
{
    NSLog(@"InMobiPlugin :: interstitialWillPresentScreen");
    
    UnityPause(true);
    UnitySendMessage("~OInMobiHandler", "interstitialWillPresentScreen", "");
}

- (void)interstitialWillDismissScreen:(IMInterstitial *)ad
{
    NSLog(@"InMobiPlugin :: interstitialWillDismissScreen");
    
    UnityPause(false);
    UnitySendMessage("~OInMobiHandler", "interstitialWillDismissScreen", "");
}

- (void)interstitialDidDismissScreen:(IMInterstitial *)ad
{
    NSLog(@"InMobiPlugin :: interstitialDidDismissScreen");
    
    UnitySendMessage("~OInMobiHandler", "interstitialDidDismissScreen", "");
}

- (void)interstitialWillLeaveApplication:(IMInterstitial *)ad
{
    NSLog(@"InMobiPlugin :: interstitialWillLeaveApplication");
    
    UnitySendMessage("~OInMobiHandler", "interstitialWillLeaveApplication", "");
}

@end


static InMobiPlugin *inMobiPlugin = nil;

extern "C"
{
    void _InitInMobi(const char* adUnitID)
    {
        if (inMobiPlugin == nil)
			inMobiPlugin = [[InMobiPlugin alloc] init];
        
        [inMobiPlugin initInMobi: adUnitID];
    }
    
    void _CreateBannerAd(const char* adUnitID)
    {
        if (inMobiPlugin == nil)
			inMobiPlugin = [[InMobiPlugin alloc] init];
        
        [inMobiPlugin createBannerAd: adUnitID];
    }
    
    void _CreateBottomBannerAd(const char* adUnitID)
    {
        if (inMobiPlugin == nil)
			inMobiPlugin = [[InMobiPlugin alloc] init];
        
        [inMobiPlugin createBottomBannerAd: adUnitID];
    }
    
    void _RemoveBannerAd()
    {
        if (inMobiPlugin == nil)
			inMobiPlugin = [[InMobiPlugin alloc] init];
        
        [inMobiPlugin removeBannerAd];
    }
    
    void _CreateInterstitialAd(const char* adUnitID)
    {
        if (inMobiPlugin == nil)
			inMobiPlugin = [[InMobiPlugin alloc] init];
        
        [inMobiPlugin createInterstitialAd: adUnitID];
    }
    
    bool _IsInterstitialAdAvailable()
    {
        if (inMobiPlugin == nil)
			inMobiPlugin = [[InMobiPlugin alloc] init];
        
        return [inMobiPlugin isInterstitialAdAvailable];
    }
    
    void _ShowInterstitialAd()
    {
        if (inMobiPlugin == nil)
			inMobiPlugin = [[InMobiPlugin alloc] init];
        
        [inMobiPlugin showInterstitialAd];
    }
}
﻿using UnityEngine;
using System.Collections;

public class NativeTestScene : TestScene
{
	public OTextButton m_nativeAlertButton;
	public OTextButton m_facebookShareButton;
	public OTextButton m_tweetButton;
	public OTextButton m_appStoreButton;
	public OTextButton m_fanPageButton;
	
	public override void Start() 
	{	base.Start();

		if(m_nativeAlertButton == null)
		{	Debug.LogWarning("NativeTestScene :: Assign ShowAlertButton!");
		}

		if(m_facebookShareButton == null)
		{	Debug.LogWarning("NativeTestScene :: Assign FacebookShareButton!");
		}

		if(m_tweetButton == null)
		{	Debug.LogWarning("NativeTestScene :: Assign TweetButton!");
		}

		if(m_appStoreButton == null)
		{	Debug.LogWarning("NativeTestScene :: Assign AppStoreButton!");
		}

		if(m_fanPageButton == null)
		{	Debug.LogWarning("NativeTestScene :: Assign FanPageButton!");
		}
	}
	
	public override void OnButtonUp(OButton button) 
	{	
		if(button == m_nativeAlertButton)
		{	OAlertHandler.OnAlertDismissed += nativeAlertDismissed;
			OAlertHandler.OnAlertCanceled += nativeAlertCanceled;
			OAlertHandler.Instance.createAlert("Title", "This is a native alert", "Okay");
		}	
		else if(button == m_facebookShareButton)
		{	OShareHandler.Instance.facebookShare("Sharing this on Facebook");
		}	
		else if(button == m_tweetButton)
		{	OShareHandler.Instance.tweet("Sharing this on Twitter");
		}	
		else if(button == m_appStoreButton)
		{	
			#if UNITY_IPHONE
			OAppStoreHandler.Instance.openAppInStore("606080169");
			#endif

			#if UNITY_ANDROID
			OAppStoreHandler.Instance.openAppInStore("org.orangenose.games");
			#endif
		}	
		else if(button == m_fanPageButton)
		{	OWebViewHandler.Instance.openURL("https://www.facebook.com/OrangenoseStudio");
		}
	}

	void nativeAlertDismissed()
	{	Debug.Log("NativeTestScene :: nativeAlertDismissed");

		OAlertHandler.OnAlertDismissed -= nativeAlertDismissed;
		OAlertHandler.OnAlertCanceled -= nativeAlertCanceled;
	}

	void nativeAlertCanceled()
	{	
		Debug.Log("NativeTestScene :: nativeAlertCanceled");

		OAlertHandler.OnAlertDismissed -= nativeAlertDismissed;
		OAlertHandler.OnAlertCanceled -= nativeAlertCanceled;
	}
}

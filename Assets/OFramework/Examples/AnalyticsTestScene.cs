﻿using UnityEngine;
using System.Collections;

public class AnalyticsTestScene : TestScene
{
	public OTextButton m_sendScreenButton;
	public OTextButton m_sendScreenOnceButton;
	public OTextButton m_sendEventButton;
	public OTextButton m_sendEventOnceButton;	
	public OTextButton m_sendTimedEventButton;	
	public OTextButton m_sendTimedEventOnceButton;
	public OTextButton m_sendSocialInteractionButton;	
	public OTextButton m_sendSocialInteractionOnceButton;
	public OTextButton m_sendPurchaseTransactionButton;	
	public OTextButton m_sendPurchaseTransactionOnceButton;
	public OTextButton m_sendCrashButton;
	public OTextButton m_sendCrashFatalButton;
	
	public override void Start() 
	{	base.Start();

		if(m_sendScreenButton == null)
		{	Debug.LogWarning("AnalyticsTestScene :: Assign SendScreenButton!");
		}

		if(m_sendScreenOnceButton == null)
		{	Debug.LogWarning("AnalyticsTestScene :: Assign SendScreenOnceButton!");
		}

		if(m_sendEventButton == null)
		{	Debug.LogWarning("AnalyticsTestScene :: Assign SendEventButton!");
		}

		if(m_sendEventOnceButton == null)
		{	Debug.LogWarning("AnalyticsTestScene :: Assign SendEventOnceButton!");
		}

		if(m_sendTimedEventButton == null)
		{	Debug.LogWarning("AnalyticsTestScene :: Assign SendTimedEventButton!");
		}

		if(m_sendTimedEventOnceButton == null)
		{	Debug.LogWarning("AnalyticsTestScene :: Assign SendTimedEventOnceButton!");
		}

		if(m_sendSocialInteractionButton == null)
		{	Debug.LogWarning("AnalyticsTestScene :: Assign SendSocialInteractionButton!");
		}

		if(m_sendSocialInteractionOnceButton == null)
		{	Debug.LogWarning("AnalyticsTestScene :: Assign SendSocialInteractionOnceButton!");
		}

		if(m_sendPurchaseTransactionButton == null)
		{	Debug.LogWarning("AnalyticsTestScene :: Assign SendPurchaseTransactionButton!");
		}

		if(m_sendPurchaseTransactionOnceButton == null)
		{	Debug.LogWarning("AnalyticsTestScene :: Assign SendPurchaseTransactionOnceButton!");
		}

		if(m_sendCrashButton == null)
		{	Debug.LogWarning("AnalyticsTestScene :: Assign SendCrashButton!");
		}

		if(m_sendCrashFatalButton == null)
		{	Debug.LogWarning("AnalyticsTestScene :: Assign SendCrashFatalButton!");
		}
	}

	public override void OnButtonUp(OButton button) 
	{	
		if(button == m_sendScreenButton)
		{	OGAHandler.Instance.SendScreen(Application.loadedLevelName);
			OGAHandler.Instance.Dispatch();
		}

		else if(button == m_sendScreenOnceButton)
		{	OGAHandler.Instance.SendScreenOnce(Application.loadedLevelName+"Once");
			OGAHandler.Instance.Dispatch();
		}

		else if(button == m_sendEventButton)
		{	OGAHandler.Instance.SendEvent("TEST", "ButtonPress", "SendEventButton", 0.0f);
			OGAHandler.Instance.Dispatch();
		}

		else if(button == m_sendEventOnceButton)
		{	OGAHandler.Instance.SendEventOnce("TEST", "ButtonPress", "SendEventOnceButton", 0.0f);
			OGAHandler.Instance.Dispatch();
		}

		else if(button == m_sendTimedEventButton)
		{	OGAHandler.Instance.SendTimedEvent("TEST", 0.5f, "ButtonPress", "SendTimedEventButton");
			OGAHandler.Instance.Dispatch();
		}

		else if(button == m_sendTimedEventOnceButton)
		{	OGAHandler.Instance.SendTimedEvent("TEST", 0.5f, "ButtonPress", "SendTimedEventOnceButton");
			OGAHandler.Instance.Dispatch();
		}

		else if(button == m_sendSocialInteractionButton)
		{	OGAHandler.Instance.SendSocialInteraction("Facebook", "ButtonPress", "SendSocialInteractionButton");
			OGAHandler.Instance.Dispatch();
		}

		else if(button == m_sendSocialInteractionOnceButton)
		{	OGAHandler.Instance.SendSocialInteraction("Facebook", "ButtonPress", "SendSocialInteractionOnceButton");
			OGAHandler.Instance.Dispatch();
		}	

		else if(button == m_sendPurchaseTransactionButton)
		{	OGAHandler.Instance.SendPurchaseTransaction("TestTranscationID", "TestProductName", "TestProductSKU", "TEST", 0.99f, 1, "USD");
			OGAHandler.Instance.Dispatch();
		}
		
		else if(button == m_sendPurchaseTransactionOnceButton)
		{	OGAHandler.Instance.SendPurchaseTransactionOnce("TestTranscationIDOnce", "TestProductName", "TestProductSKU", "TEST", 0.99f, 1, "USD");
			OGAHandler.Instance.Dispatch();
		}

		else if(button == m_sendCrashButton)
		{	OGAHandler.Instance.SendCrash("Crash", false);
			OGAHandler.Instance.Dispatch();
		}

		else if(button == m_sendCrashFatalButton)
		{	OGAHandler.Instance.SendCrash("FatalCrash", true);
			OGAHandler.Instance.Dispatch();
		}

		else if(button.m_buttonId == "btn_totaltimespent")
		{	float totalPlaytime = ODBMgr.Instance.getFloat(ODBKeys.KPI_TOTAL_TIME_SPENT);
			OGAHandler.Instance.SendEvent(OGAKeys.CATEGORY_KPI, OGAKeys.ACTION_APP_FLOW_INTERRUPT, OGAKeys.LABEL_TOTAL_PLAY_TIME, totalPlaytime);
			OGAHandler.Instance.Dispatch();
		}

	}
}

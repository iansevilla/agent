﻿using UnityEngine;
using System.Collections;

public class OAlphaAnimation : OAnimation
{

	public float alphaInitial = 1f;
	public float alphaTarget = 1f;

	public override void ResetAnimation ()
	{
		TextMesh textMesh = gameObject.GetComponent<TextMesh> ();

		if (textMesh != null) {
			Color color = textMesh.color;
			color.a = alphaInitial;
			textMesh.color = color;
		}

		SpriteRenderer spriteRen = gameObject.GetComponent<SpriteRenderer> ();

		if (spriteRen != null) {
			Color color = spriteRen.color;
			color.a = alphaInitial;
			spriteRen.color = color;
		}
	}

	public override void DoAnimation ()
	{
		ApplyBaseSettings (LeanTween.alpha (gameObject, alphaTarget, animationDuration));
	}
}

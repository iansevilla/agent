﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class OTunedVariable
{
	public string name;
	public float value;
}

[ExecuteInEditMode]
public class OTuneMgr : Singleton<OTuneMgr> {

	public float m_tunedOffset = 0.1f;
	public List<OTunedVariable> m_listTunedVars;

	// Use this for initialization
	void Awake () {
		foreach( OTunedVariable tunable in m_listTunedVars )
		{	if( !PlayerPrefs.HasKey( tunable.name ) )
				PlayerPrefs.SetFloat( tunable.name, tunable.value );
		}
		foreach( OTunedVariable tunable in m_listTunedVars )
		{	tunable.value = PlayerPrefs.GetFloat( tunable.name );
		}
	}

	public void SetFloat (string name, float value) {
		foreach( OTunedVariable tunable in m_listTunedVars )
		{	if(tunable.name == name)
			{	tunable.value = value;
				PlayerPrefs.SetFloat( tunable.name, tunable.value );
			}
		}
	}

	public float GetFloat (string name) {
		foreach( OTunedVariable tunable in m_listTunedVars )
		{	if( tunable.name == name )
			{	return PlayerPrefs.GetFloat(name);
			}
		}
		return 0f;
	}

	void Update(){
		if( Application.isEditor && !Application.isPlaying )
		{	foreach( OTunedVariable tunable in m_listTunedVars )
			{	if( !Mathf.Approximately(PlayerPrefs.GetFloat( tunable.name ), tunable.value) )
				{	PlayerPrefs.SetFloat( tunable.name, tunable.value );
				}
				else
				{	tunable.value = PlayerPrefs.GetFloat( tunable.name );
				}
			}
		}
	}
}

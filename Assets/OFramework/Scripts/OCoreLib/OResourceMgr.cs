﻿using UnityEngine;
using System.Collections;

public class OResourceMgr : Singleton<OResourceMgr> 
{
	private Hashtable m_spritesCache;

	public AudioClip LoadAudioClip( string clipName ) 
	{	return (AudioClip)Resources.Load (clipName) as AudioClip;
	}

	public GameObject LoadPrefab( string prefabName ) 
	{	return (GameObject)Resources.Load (prefabName);
	}

	// ATTENTION: spritsheet needs to be initialized first and foremost, initSpritesheet only accepts Multiple sprites
	public void initSpritesheet(string spritesheetName) 
	{		
		if (m_spritesCache == null) 
		{	m_spritesCache = new Hashtable();		
		}
		
		Sprite[] spritesheet = Resources.LoadAll<Sprite>(spritesheetName);
		
		foreach (Sprite sprite in spritesheet) 
		{	
			// sprites with same names are ignored
			if(!m_spritesCache.Contains(sprite.name))
			{	m_spritesCache.Add(sprite.name, sprite);
			}
		}
	}
	
	// ATTENTION: always clear spritesheet cache when changing Scenes
	public void clearSpritesheetCache()
	{	m_spritesCache.Clear();
	}	

	public Sprite getSprite(string name) 
	{	if (m_spritesCache.Contains (name)) 
		{	return (Sprite) m_spritesCache[name];		
		}
		
		return null;
	}
}

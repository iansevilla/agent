﻿using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;

public class OGAKeys
{
	public const string CATEGORY_KPI = "KPI";

	public const string ACTION_APP_FLOW_OPEN = "AppOpen";
	public const string ACTION_APP_FLOW_RESUME = "Resume";
	public const string ACTION_APP_FLOW_INTERRUPT = "Interrupt";
	
	public const string LABEL_TOTAL_PLAY_TIME = "TotalPlayTime";
	public const string LABEL_TOTAL_PLAY_TIME_30 = "TotalPlayTime30";
	public const string LABEL_TOTAL_PLAY_TIME_60 = "TotalPlayTime60";
	public const string LABEL_TOTAL_PLAY_TIME_120 = "TotalPlayTime120";
	public const string LABEL_TOTAL_PLAY_TIME_240 = "TotalPlayTime240";
	public const string LABEL_TOTAL_PLAY_TIME_500 = "TotalPlayTime500";
	public const string LABEL_TOTAL_PLAY_TIME_1000 = "TotalPlayTime1000";
}

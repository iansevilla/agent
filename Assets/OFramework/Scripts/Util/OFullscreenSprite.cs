﻿using UnityEngine;
using System.Collections;

public class OFullscreenSprite : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public static GameObject InstantiateFullscreenSprite(string name, Color color, string sortingLayerName = "Default", int sortingOrder = 0) {
		GameObject go = new GameObject(name);
		
		go.transform.localScale = new Vector3(OGameMgr.Instance.CAMERA_SIZE * 4, OGameMgr.Instance.CAMERA_SIZE * 4);
		
		SpriteRenderer sr = go.AddComponent<SpriteRenderer>();
		sr.sprite = SolidSprite.sprite;
		sr.color = color;
		sr.sortingLayerName = sortingLayerName;
		sr.sortingOrder = sortingOrder;
		
		return go;
	}
	
	
}

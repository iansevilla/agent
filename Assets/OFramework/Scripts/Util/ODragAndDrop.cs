﻿/*
	ODragAndDrop by rsui. (09/02/14)
 
	usage: 
	Attach script to object  to be able to drag and drop
	Note that this only supports single object drag and drop
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof(PolygonCollider2D))]
public class ODragAndDrop: OStateMachine<ODragAndDrop.State>
{
	public enum State
	{
		IDLE,
		DRAGSTARTING,
		DRAGSTART,
		DRAGIDLE,
		DRAGMOVING,
		DRAGRELEASED,
		LOCKED
	}
	
	public static bool OneItemOnly = true;
	public static ODragAndDrop HeldObject { get; set; }

	// Use this for initialization
	void Start () 
	{
		_polygonCollider = this.GetComponent<PolygonCollider2D> ();
	}

	protected override OStateMachineTransitionListener<ODragAndDrop.State>[] 
		InitializeListeners ()
	{
		return this.GetComponents<ODragAndDropListener>();
	}

	#region Private Fields
	private PolygonCollider2D _polygonCollider;
	private Vector2 _previousInputPosition;
	private int _instanceDistanceThreshold = 3;

	private static bool _isTouchDown = false;
	private static bool _hasTouchedDown = false;
	#endregion

	void Update()
	{
		// Precache Input State
		if(!_hasTouchedDown)
			_isTouchDown = OInputMgr.Instance.isTouchDown ();
		_hasTouchedDown = true;
	}

	void LateUpdate()
	{
		if (Time.timeScale == 0) return;

		_hasTouchedDown = false;

		if (m_currentState == State.IDLE) 
		{
			this._polygonCollider.enabled = false;
			if(_isTouchDown) 
			{
				float distance = Vector2.Distance (
					OInputMgr.Instance.getTouchDownPos(),
					this.gameObject.transform.position);
				
				if(distance < _instanceDistanceThreshold)
				{
					ChangeState(State.DRAGSTARTING);
					this._polygonCollider.enabled = true;
				}
			}
		}

		if (m_currentState == State.DRAGSTARTING) 
		{
			if(OneItemOnly && 
			   HeldObject == null && 
			   OInputMgr.Instance.isTouch () && 
			   OInputMgr.Instance.isSpriteTouch (gameObject))
			{
				HeldObject = this;
				ChangeState(State.DRAGSTART);
			}
			else ChangeState(State.IDLE);
		}

		if (m_currentState == State.DRAGSTART) 
			ChangeState( OInputMgr.Instance.isTouch () && 
           		OInputMgr.Instance.isSpriteTouch (gameObject) ? State.DRAGIDLE: State.DRAGRELEASED);

		
		if (m_currentState == State.DRAGIDLE || m_currentState == State.DRAGMOVING) 
		{
			Vector3 touchInputPosition = Camera.main.ScreenToWorldPoint( 
				new Vector3(OInputMgr.Instance.getTouchScreenPos().x, 
			            	OInputMgr.Instance.getTouchScreenPos().y, 0 ));

			this.transform.position = new Vector3(touchInputPosition.x, touchInputPosition.y, 0.0f);

			if(OInputMgr.Instance.isTouchUp()) 
				ChangeState(State.DRAGRELEASED);
		}

		if (m_currentState == State.DRAGRELEASED) 
		{
			ChangeState(State.IDLE);
			HeldObject = null;			
		}
	}
}
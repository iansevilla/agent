﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CivilManager : MonoBehaviour {

	public static CivilManager instance;

	public Transform Point1_1;
	public Transform Point1_2;
	public Transform Point1_3;
	public Transform Point2_1;
	public Transform Point2_2;
	public Transform Point2_3;
	public Transform Point3_1;
	public Transform Point3_2;
	public Transform Point3_3;

	public GameObject civilPrefab;

	public List<Transform> m_TransformPoints = new List<Transform>();

	// Use this for initialization
	void Start () {
		instance = this;
		m_TransformPoints.Add (Point1_1);
		m_TransformPoints.Add (Point1_2);
		m_TransformPoints.Add (Point1_3);
		m_TransformPoints.Add (Point2_1);
		m_TransformPoints.Add (Point2_2);
		m_TransformPoints.Add (Point2_3);
		m_TransformPoints.Add (Point3_1);
		m_TransformPoints.Add (Point3_2);
		m_TransformPoints.Add (Point3_3);


	}
	
	// Update is called once per frame
	void Update () {

		if(Input.GetKeyDown(KeyCode.P))
		{
			SpawnCivil();
		}
		if(Input.GetKeyDown(KeyCode.O))
		{
			DestroyAllCivil();
		}



	
	}

	public void SpawnCivil()
	{
		GameObject civilObject = Instantiate (civilPrefab, m_TransformPoints [Random.Range (0, 8)].transform.position, Quaternion.identity) as GameObject;
		civilObject.transform.parent = this.transform;
	}
	public void DestroyAllCivil()
	{
		foreach(GameObject go in GameObject.FindGameObjectsWithTag("Civil"))
		{
			Destroy(go.gameObject);
		}
	}

	public void EnablePreloadedCivil()
	{
		foreach(GameObject go in GameObject.FindGameObjectsWithTag("Civil"))
		{
			go.SetActive(true);
		}
	}

}
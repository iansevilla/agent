﻿using UnityEngine;
using System.Collections;

public class CivilScript : MonoBehaviour {

	public static CivilScript instance;

	public float m_civilSpeed;
	public float m_civilDirection;

	public bool	 m_civilIsAlive;

	public SpriteRenderer m_civilSprite;

	public GameObject CivilAnimator;
	//public GameObject AgentFaceAnimator;
	Animator Anim;
	//Animator AnimFace;

	// Use this for initialization
	void Start () {
		instance = this;

		m_civilDirection = -1f;
		m_civilSpeed = Random.Range(CivilConfig.CIVIL_SPEED, CivilConfig.CIVIL_MAX_SPEED);

		m_civilIsAlive = true;

		Anim = CivilAnimator.gameObject.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if(m_civilDirection < 0)
		{
			m_civilSprite.transform.rotation = Quaternion.Euler (0, 0, 0);
			transform.Translate(Vector3.left * Time.deltaTime * m_civilSpeed);
		}
		if(m_civilDirection >= 0)
		{
			m_civilSprite.transform.rotation = Quaternion.Euler (0, 180, 0);
			transform.Translate(Vector3.right * Time.deltaTime * m_civilSpeed);
		}

		if (!m_civilIsAlive)
			Destroy(this.gameObject);
	
	}
	void OnTriggerEnter(Collider other) 
	{
		//Debug.Log("other " + other);
		if(other.tag == "BoarderLeft")
		{
//			Debug.Log("BoarderLeft");
			m_civilDirection = Random.Range(-1, 1);
		}
		if(other.tag == "BoarderRight")
		{
//			Debug.Log("BoarderLeft");
			m_civilDirection = -1;
		}

		if(other.tag == "BoarderDestroy")
		{
			Destroy(this.gameObject);
		}

	}

	void HasPlayerDied()
	{
		Debug.Log("DEAD DEAD DEAD DEAD");

		//yield return new WaitForSeconds (.1f);
		OGameMgr.Instance.Fader.Fade(Color.red, 0, 1f, 1f, deactivateOnFadeEnd: false, onFadeEnd: OnFadeEndAgentScript);
	
	}
	
	void OnFadeEndAgentScript()
	{
		Debug.Log("DEAD DEAD DEAD DEAD2222222");
		OGameMgr.Instance.Fader.Fade(Color.red, 1f, 0, 1f);
		OGameMgr.Instance.ShowPopup("SResultPopup");
	}

	public bool Get_isAliveCivil()
	{
		return m_civilIsAlive;
	}
	public void Set_isAliveCivil(bool isAlive)
	{
		HasPlayerDied ();
		m_civilIsAlive = isAlive;
	}
}

﻿
public class AgentConfig  {


	//Agent
	public const float 		AGENT_SPEED 			= 2f;
	public const float		AGENT_MAX_SPEED 		= 3f;
	
	public const float 		AGENT_MAX_SHOOT_SPEED 	= .8f;
	public const float 		AGENT_SHOOT_SPEED 		= .8f;

	public const float 		AGENT_WALK_TIME 		= 5f;
	public const float 		AGENT_MAX_WALK_TIME 	= 8f;

	//Player
	public const int 		PLAYER_MAX_BULLET 		= 6 ;

	public const string 	AGENT_SFX_SHOOT 		= "SFX_SHOOT";
	public const string 	AGENT_SFX_RELOAD 		= "SFX_RELOAD";
	public const string 	AGENT_SFX_SHOUT 		= "SFX_SHOUT";
}

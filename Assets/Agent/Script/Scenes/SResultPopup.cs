using UnityEngine;
using System.Collections;

public class SResultPopup : OPopupScene {

	public OLabel m_lblScore;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		m_lblScore.UpdateLabel ("Score: " + PlayerScript.instance.Get_PlayerScore ());
	}


	public override void OnButtonUp (OButton button)
	{
		if (button.m_buttonId == "btn_mainScreen")
		{	OGameMgr.Instance.ShowPopup("SHomePopup");
			PlayerScript.instance.ResetPlayerValues();
			AgentManager.instance.ResetAgentManagerValues();
		}

		else if (button.m_buttonId == "btn_InGame")
		{	OGameMgr.Instance.HidePopup();

		}
	}
}

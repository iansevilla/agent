﻿using UnityEngine;
using System.Collections;

public class SPausePopup : OPopupScene {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void OnButtonUp (OButton button)
	{
		if (button.m_buttonId == "btn_resume")
		{	OGameMgr.Instance.SetPaused(false);
			OGameMgr.Instance.HidePopup();
		}

		if (button.m_buttonId == "btn_home")
		{	
			OGameMgr.Instance.ShowPopup("SHomePopup");
		}
	}
}

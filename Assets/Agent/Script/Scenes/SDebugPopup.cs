﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SDebugPopup : OPopupScene {
	
	public List<OLabel> m_listName;
	public List<OLabel> m_listValue;

	// Use this for initialization
	void Start () {
		int x = 0;
		foreach(OLabel lbl in m_listName)
		{	if( x < OTuneMgr.Instance.m_listTunedVars.Count)
			{	OTunedVariable tunedvar = OTuneMgr.Instance.m_listTunedVars[x];
				m_listName[x].UpdateLabel("" + tunedvar.name);
				m_listValue[x].UpdateLabel("" + tunedvar.value);
			}
			else
			{	m_listName[x].UpdateLabel("N/A");
				m_listValue[x].UpdateLabel("");
			}
			x++;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void OnButtonUp (OButton button)
	{
		if (button.m_buttonId == "btn_back")
		{	//OGameMgr.Instance.ShowPopup("SHomePopup");
			OGameMgr.Instance.HidePopup();
		}

		else if (button.m_buttonId == "btn_banner")
		{	OInMobiHandler.Instance.createBannerAd();
		}

		else if (button.m_buttonId == "btn_interstitial")
		{	
		}

		else if (button.m_buttonId == "btn_sendevent")
		{	OGAHandler.Instance.SendEvent(OGAKeys.CATEGORY_KPI, OGAKeys.ACTION_APP_FLOW_INTERRUPT, OGAKeys.LABEL_TOTAL_PLAY_TIME, 1.0f);
		}

		else if (button.m_buttonId.Contains("minus"))
		{	int x = 0;
			foreach(OTunedVariable tunedvar in OTuneMgr.Instance.m_listTunedVars)
			{	if(button.m_buttonId.Contains("" + (x+1)))
				{	OTuneMgr.Instance.SetFloat(tunedvar.name, tunedvar.value - OTuneMgr.Instance.m_tunedOffset);
					m_listValue[x].UpdateLabel("" + tunedvar.value);
					Debug.Log ("Update Minus : " + tunedvar.value);
				}
				x++;
			}
		}

		else if (button.m_buttonId.Contains("plus"))
		{	int x = 0;
			foreach(OTunedVariable tunedvar in OTuneMgr.Instance.m_listTunedVars)
			{	if(button.m_buttonId.Contains("" + (x+1)))
				{	OTuneMgr.Instance.SetFloat(tunedvar.name, tunedvar.value + OTuneMgr.Instance.m_tunedOffset);
					m_listValue[x].UpdateLabel("" + tunedvar.value);
					Debug.Log ("Update Plus : " + tunedvar.value);
				}
				x++;
			}
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class SRoundPopup : OPopupScene {

	public static SRoundPopup instance;
	
	public OLabel m_lblRound;

	private int m_currentRound;

	// Use this for initialization
	void Start () {
		instance = this;

	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnEnable()
	{
		//OGameMgr.Instance.Fader.Fade(Color.black, 1f, 0, .01f);
		/*if(m_currentRound == 0)
		{
			m_currentRound = PlayerScript.instance.m_nRound + 1;
		}
		else
			m_currentRound += 1;
		//m_lblRound.UpdateLabel ("Round: " + m_currentRound);
		*/

		PlayerScript.instance.m_nRound += 1;
		AgentManager.instance.DisableAllAgent ();
		PlayerScript.instance.Set_PlayerBullets(6);
		StartCoroutine (WaitFewSec ());
	}

	IEnumerator WaitFewSec()
	{
		yield return new WaitForSeconds (.01f);
		//OGameMgr.Instance.Fader.Fade(Color.black, 0, 1f, .01f, deactivateOnFadeEnd: false, onFadeEnd: OnFadeEndRound);
		OnFadeEndRound ();
	}

	void OnFadeEndRound()
	{
		//OGameMgr.Instance.Fader.Fade(Color.black, 1f, 0, .01f);
		Debug.Log ("NEXT ROUND: " + PlayerScript.instance.m_nRound);
		AgentManager.instance.SetRound (PlayerScript.instance.m_nRound);
		AgentManager.instance.m_isRoundStart = true;
		AgentManager.instance.m_bHasKilledAllTheAgent = false;
		OGameMgr.Instance.HidePopup();

		//AgentManager.instance.en
	}
}

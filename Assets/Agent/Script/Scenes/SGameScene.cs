﻿using UnityEngine;
using System.Collections;

public class SGameScene : OGameScene {
	
	// Use this for initialization
	void Start () {
		OGameMgr.Instance.ShowPopup("SSplashPopup");
		//OGameMgr.Instance.ShowPopup("SHomePopup");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void OnButtonUp (OButton button)
	{
		if (button.m_buttonId == "btn_pause")
		{	OGameMgr.Instance.SetPaused(true);
			OGameMgr.Instance.ShowPopup("SPausePopup");
		}

		else if (button.m_buttonId == "btn_mainScreen")
		{	OGameMgr.Instance.ShowPopup("SHomePopup");
		}

		else if (button.m_buttonId == "btn_resultScreen")
		{	OGameMgr.Instance.ShowPopup("SResultPopup");
		}
		else if (button.m_buttonId == "btn_debug")
		{	OGameMgr.Instance.ShowPopup("SDebugPopup");
		}
		else if (button.m_buttonId == "spawn")
		{	
			AgentManager.instance.CheatSpawn();
		}
	}
}

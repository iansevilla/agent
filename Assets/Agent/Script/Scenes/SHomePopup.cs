﻿using UnityEngine;
using System.Collections;

public class SHomePopup : OPopupScene {

	public AudioClip m_sampleBGM;

	public OSpriteToggleButton m_toggleBGM;
	public OSpriteToggleButton m_toggleSFX;

	void OnEnable()
	{
		OAudioMgr.Instance.PlayBGM (m_sampleBGM);

		m_toggleBGM.Toggle( ODBMgr.Instance.getBool( ODBKeys.GAME_BGM_ENABLED ) );
		m_toggleSFX.Toggle( ODBMgr.Instance.getBool( ODBKeys.GAME_SFX_ENABLED ) );

		OAudioMgr.Instance.SetEnabledBGM (m_toggleBGM.m_isToggled);
		OAudioMgr.Instance.SetEnabledSFX (m_toggleSFX.m_isToggled);
	}

	public override void OnButtonUp (OButton button)
	{
		if (button.m_buttonId == "btn_play")
		{	
			//OGameMgr.Instance.HidePopup();
			PlayerScript.instance.m_nRound = 0;
			OGameMgr.Instance.Fader.Fade(Color.black, 0, 1f, 1f, deactivateOnFadeEnd: false, onFadeEnd: OnFadeEndHome);
		}

		else if (button.m_buttonId == "btn_bgm")
		{	OAudioMgr.Instance.SetEnabledBGM (m_toggleBGM.m_isToggled);
			ODBMgr.Instance.setBool( ODBKeys.GAME_BGM_ENABLED, m_toggleBGM.m_isToggled );
		}

		else if (button.m_buttonId == "btn_sfx")
		{	OAudioMgr.Instance.SetEnabledSFX (m_toggleSFX.m_isToggled);
			ODBMgr.Instance.setBool( ODBKeys.GAME_SFX_ENABLED, m_toggleSFX.m_isToggled );
		}

		else if (button.m_buttonId == "btn_aboutScreen")
		{	OGameMgr.Instance.ShowPopup("SAboutPopup");
		}

		else if (button.m_buttonId == "btn_debug")
		{	OGameMgr.Instance.ShowPopup("SDebugPopup");
		}
	}
	void OnFadeEndHome()
	{
		OGameMgr.Instance.ShowPopup("SRoundPopup");
		OGameMgr.Instance.Fader.Fade(Color.black, 1f, 0, .01f);
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AgentManager : MonoBehaviour {

	public static AgentManager instance;

	public GameObject 	m_AgentPrefab;
	public GameObject[] m_AgentObjets;

	//public GameObject m_Agent1;
	//public GameObject m_Agent2;
	//public GameObject m_Agent3;
	//public GameObject m_Agent4;
	//public GameObject m_Agent5;

	public Transform m_Point1_1;
	public Transform m_Point1_2;
	public Transform m_Point1_3;
	public Transform m_Point2_1;
	public Transform m_Point2_2;
	public Transform m_Point2_3;
	public Transform m_Point3_1;
	public Transform m_Point3_2;
	public Transform m_Point3_3;

	public AudioClip m_sfx_Shoot;

	public bool m_isRoundStart = false;

	public int m_nAgentNeedToKill = 5;

	public bool m_bHasKilledAllTheAgent = false;

	private List<Transform> m_TransformPoints = new List<Transform>();

	//public List<AgentInfo> m_AgentList = new List<AgentInfo>();

	// Use this for initialization
	void Start () {
		instance = this;
		m_TransformPoints.Add (m_Point1_1);
		m_TransformPoints.Add (m_Point1_2);
		m_TransformPoints.Add (m_Point1_3);
		m_TransformPoints.Add (m_Point2_1);
		m_TransformPoints.Add (m_Point2_2);
		m_TransformPoints.Add (m_Point2_3);
		m_TransformPoints.Add (m_Point3_1);
		m_TransformPoints.Add (m_Point3_2);
		m_TransformPoints.Add (m_Point3_3);

		DisableAllAgent ();
	}
	
	// Update is called once per frame
	void Update () {

		if (!PlayerScript.instance.Get_IsPlayerAlive())
						return;

		if(m_isRoundStart && !m_bHasKilledAllTheAgent)
		{
			//ICS REMOVE
			//return;
			MouseClick ();
			TapSelect ();
		}
		if(Input.GetKeyDown(KeyCode.S))
		{
			CheatSpawn();
		}

	}

	void LateUpdate()
	{
		
		if (m_nAgentNeedToKill <= 0) {
			m_bHasKilledAllTheAgent = true;
		}
	}

	public void CheatSpawn()
	{
		m_isRoundStart = true;
		SetRound(1);
		PlayerScript.instance.Set_PlayerBullets(6);
	}

	public void SetRound(int currentRound)
	{

		switch(currentRound)
		{
		case 1:
			m_nAgentNeedToKill = 1;
			SpawnNewAgent();
			CivilManager.instance.SpawnCivil();
			CivilManager.instance.SpawnCivil();
			break;
		case 2:
			m_nAgentNeedToKill = 1;
			SpawnNewAgent();
			CivilManager.instance.SpawnCivil();
			CivilManager.instance.SpawnCivil();
			break;
		case 3:
			m_nAgentNeedToKill = 1;
			SpawnNewAgent();
			CivilManager.instance.SpawnCivil();
			CivilManager.instance.SpawnCivil();
			CivilManager.instance.SpawnCivil();
			break;
		case 4:
			m_nAgentNeedToKill = 2;
			//SpawnNewAgent(m_Agent3);
			//SpawnNewAgent(m_Agent2);
			CivilManager.instance.SpawnCivil();
			CivilManager.instance.SpawnCivil();
			CivilManager.instance.SpawnCivil();
			break;
		case 98:
			m_nAgentNeedToKill = 3;
			//SpawnNewAgent(m_Agent1);
			//SpawnNewAgent(m_Agent2);
			//SpawnNewAgent(m_Agent3);
			//SpawnNewAgent(m_Agent4);
			break;
		case 99:
			m_nAgentNeedToKill = 5;
			//SpawnNewAgent(m_Agent1);
			//SpawnNewAgent(m_Agent2);
			//SpawnNewAgent(m_Agent3);
			//SpawnNewAgent(m_Agent4);
			//SpawnNewAgent(m_Agent5);
			break;
		default:
			m_nAgentNeedToKill = 2;
			//SpawnNewAgent(m_Agent3);
			//SpawnNewAgent(m_Agent2);
			CivilManager.instance.SpawnCivil();
			CivilManager.instance.SpawnCivil();
			CivilManager.instance.SpawnCivil();
			break;
		}

	}

	public void DisableAllAgent()
	{
		//CivilManager.instance.DestroyAllCivil();
		//m_Agent1.SetActive (false);
		//m_Agent2.SetActive (false);
		//m_Agent3.SetActive (false);
		//m_Agent4.SetActive (false);
		//m_Agent5.SetActive (false);
	}
	public void resetAllAgent()
	{
		/*
		go_AgentPosition.GetComponent<AgentScript>().ResetAgentValues();
		
		go_AgentPosition.transform.position = new Vector3 (m_TransformPoints [Random.Range (0, 2)].transform.position.x, 
		                                               go_AgentPosition.transform.position.y, 
	                                                   go_AgentPosition.transform.position.z);*/

		foreach(GameObject go in GameObject.FindGameObjectsWithTag("Agent"))
		{
			Destroy(go.gameObject);
		}
	}
	public void SpawnNewAgent()
	{

		GameObject agentObject = Instantiate (m_AgentPrefab, m_TransformPoints [Random.Range (0, 8)].transform.position, Quaternion.identity) as GameObject;
		agentObject.transform.parent = this.transform;

		//go_AgentPosition.SetActive(true);
		//go_AgentPosition.GetComponent<AgentScript>().ResetAgentValues();

		//go_AgentPosition.transform.position = new Vector3 (m_TransformPoints [Random.Range (0, 2)].transform.position.x, 
		                                                  // go_AgentPosition.transform.position.y, 
		                                                  // go_AgentPosition.transform.position.z);
	}

	void MouseClick() 
	{
		if (Input.GetMouseButtonDown(0)) 
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit ;
			if (Physics.Raycast (ray, out hit)) 
			{	
				AgentTouchEvents(hit);
			}
		}
	}
	void TapSelect() 
	{
		foreach (Touch touch in Input.touches) 
		{
			if ( touch.phase == TouchPhase.Began) 
			{
				Ray ray = Camera.main.ScreenPointToRay(touch.position);
				RaycastHit hit ;
				if (Physics.Raycast (ray, out hit)) 
				{	
					AgentTouchEvents(hit);
				}
			}
		}
	}

	void AgentTouchEvents(RaycastHit hit)
	{
		if (PlayerScript.instance.Get_IsOutOfBullets ())
		{
			return;
		}
		int currentBullet = PlayerScript.instance.Get_PlayerBullets ();
		//CHEAT FOR INFINITE BULLETS
		currentBullet++;
		PlayerScript.instance.Set_PlayerBullets (currentBullet);


		if(hit.collider.tag == "Agent1")
		{ 
			//ShootAgent(m_Agent1);
		}
		else if(hit.collider.tag == "Agent2")
		{ 
			//ShootAgent(m_Agent2);
		}
		else if(hit.collider.tag == "Agent3")
		{ 
			//ShootAgent(m_Agent3);
		}
		else if(hit.collider.tag == "Agent4")
		{ 
			//ShootAgent(m_Agent4);
		}
		else if(hit.collider.tag == "Agent5")
		{ 
			//ShootAgent(m_Agent5);
		}
		else if(hit.collider.tag == "Agent")
		{ 
			ShootAgent(hit.collider.gameObject);

			//ShootAgent();
		}
		else if(hit.collider.tag == "Civil")
		{ 
			ShootCivil(hit.collider.gameObject);
			
			//ShootAgent();
		}
		else if(hit.collider.tag == "Missed")
		{
			OAudioMgr.Instance.PlaySFX(m_sfx_Shoot);
		}
	}

	public void ShootCivil(GameObject go_CivilIndex)
	{
		OAudioMgr.Instance.PlaySFX(m_sfx_Shoot);
		if(go_CivilIndex.GetComponent<CivilScript>().Get_isAliveCivil())
		{
			//OCameraMgr.Instance.CameraShake(0.008f,0.002f);
			go_CivilIndex.GetComponent<CivilScript>().Set_isAliveCivil(false);
		}
	}

	public void ShootAgent(GameObject go_AgentIndex)
	{

		Debug.Log("isCanShoot: " + go_AgentIndex.GetComponent<AgentScript>().Get_IsAgentCanShoot());
		//if(go_AgentIndex.GetComponent<AgentScript>().Get_IsAgentCanShoot())
		//{
			OAudioMgr.Instance.PlaySFX(m_sfx_Shoot);
			if(go_AgentIndex.GetComponent<AgentScript>().Get_IsAgentAlive())
			{
				m_nAgentNeedToKill--;
				StartCoroutine(HasKilledAllTheAgent());
				PlayerScript.instance.Set_PlayerScore(ReactionScore(go_AgentIndex.GetComponent<AgentScript>().Get_DeathTime()));
				go_AgentIndex.GetComponent<AgentScript>().SetAgentScore(ReactionScore(go_AgentIndex.GetComponent<AgentScript>().Get_DeathTime()));
				//OCameraMgr.Instance.CameraShake(0.008f,0.002f);
				go_AgentIndex.GetComponent<AgentScript>().Set_IsAgentAlive(false);
			}
		//}
	}

	public int ReactionScore(float currentTime, bool isHeadShot = false)
	{
		int nScore = 0;

		float ftime = 0;
		float fMaxTimeToShoot = AgentConfig.AGENT_MAX_SHOOT_SPEED;

		Debug.Log (fMaxTimeToShoot);

		Debug.Log ("4 : " + (fMaxTimeToShoot/4) * 3 );
		Debug.Log ("3 : " + (fMaxTimeToShoot/4) * 2 );
		Debug.Log ("2 : " + (fMaxTimeToShoot/4) * 1 );
		Debug.Log ("1 : " + (fMaxTimeToShoot/4) * 0 );

		if ((fMaxTimeToShoot/4) * 3 <= currentTime)
			nScore = 4;
		else if ((fMaxTimeToShoot/4) * 2 <= currentTime)
			nScore = 3;

		else if ((fMaxTimeToShoot/4) * 1 <= currentTime)
			nScore = 2;

		else if ((fMaxTimeToShoot/4) * 0 <= currentTime)
			nScore = 1;


		Debug.Log ("Score: " + nScore);
		return nScore;
	}

	IEnumerator HasKilledAllTheAgent()
	{
		yield return new WaitForSeconds (.3f);
		if(m_bHasKilledAllTheAgent)
		{
			//OGameMgr.Instance.Fader.Fade(Color.black, 0, 1f, .2f, deactivateOnFadeEnd: false, onFadeEnd: OnFadeEndAgentManager);
			m_isRoundStart = false;
			OnFadeEndAgentManager();
		}
	}
	void OnFadeEndAgentManager()
	{
		OGameMgr.Instance.ShowPopup("SRoundPopup");
	}

	public void ResetAgentManagerValues()
	{
		Debug.Log("RESET AGENT MANAGER");
		m_bHasKilledAllTheAgent = false;
		m_isRoundStart = false;
		DisableAllAgent ();
	}
}

//public class AgentInfo{
//	int		agentIndex;
//	string 	agentName;
//	bool	isAlive;
//	bool	isCanShoot;
//}

﻿using UnityEngine;
using System.Collections;

public class AgentScript : MonoBehaviour {

	public static AgentScript instance;

	//Label
	public OLabel m_lblScore;

	//Sprite
	public SpriteRenderer m_AgentSprite;
	public SpriteRenderer m_Crosshair;

	//Audio
	public AudioClip m_sfx_reload;
	public AudioClip m_sfx_shoot;
	public AudioClip m_sfx_shout;

	//Agent Variables
	public float m_agentSpeed;
	public float m_agentDirection;
	float m_timeToWalk;

	//Timer
	public GameObject TimerObject;

	//TimerToShoot
	float m_timeToShot;
	float m_maxTimeToShot;

	//AgentAim
	public bool m_AgentShootOnce = false;

	//Animation
	public GameObject AgentAnimator;
	public GameObject AgentFaceAnimator;
	Animator Anim;
	Animator AnimFace;

	//temp
	bool tempCallOnce = true;

	private bool m_isAgentCanShoot = false;
	private bool m_isAgentAlive = true;

	// Use this for initialization
	void Start () {
		instance = this;

		m_agentDirection = -1f;
		m_agentSpeed = Random.Range(AgentConfig.AGENT_SPEED, AgentConfig.AGENT_MAX_SPEED);

		Anim = AgentAnimator.gameObject.GetComponent<Animator>();
		AnimFace = AgentFaceAnimator.gameObject.GetComponent<Animator> ();

		m_timeToWalk = Random.Range (AgentConfig.AGENT_WALK_TIME, AgentConfig.AGENT_MAX_WALK_TIME);

		//TimerObject.gameObject.renderer.material.SetFloat ("_Cutoff", 0.005f);

		TimerObject.SetActive(false);
		m_lblScore.gameObject.SetActive (false);
		//this.gameObject.collider.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(m_agentDirection == -1)
		{
			m_AgentSprite.transform.rotation = Quaternion.Euler (0, 0, 0);
			transform.Translate(Vector3.left * Time.deltaTime * m_agentSpeed);
		}
		if(m_agentDirection == 1)
		{
			m_AgentSprite.transform.rotation = Quaternion.Euler (0, 180, 0);
			transform.Translate(Vector3.right * Time.deltaTime * m_agentSpeed);
		}
		if(!m_isAgentAlive)
			KilledTheAgent ();

		if(Input.GetKeyDown(KeyCode.R))
		{
			ResetAgentValues();
		}
			
	}

	void FixedUpdate()
	{
		if (!m_isAgentAlive)
			return;

		m_timeToWalk -= Time.deltaTime;
		if(m_timeToWalk <= 0 && tempCallOnce)
		{
			m_timeToShot = AgentConfig.AGENT_SHOOT_SPEED;
			m_maxTimeToShot = AgentConfig.AGENT_SHOOT_SPEED;
			
			//AgentAnimation.instance.AgentStartToAim();

			Anim.SetTrigger ("AgentAimAnim");
			AnimFace.SetTrigger("AgentAimAnim");


			tempCallOnce = false;

			OAudioMgr.Instance.PlaySFX(m_sfx_reload);
			StartCoroutine(DelaytoStopSpeed());
			m_isAgentCanShoot = true;
			//set here can shoot
		}
		if(!tempCallOnce)
			AgentShot();
	}

	IEnumerator DelaytoStopSpeed()
	{
		yield return new WaitForSeconds (.5f);
		m_agentSpeed = 0f;
	}

	public void AgentShot()
	{
		m_timeToShot -= Time.deltaTime;
		
		if (m_timeToShot <= 0 && !m_AgentShootOnce)
		{
			m_AgentShootOnce = true;
			OAudioMgr.Instance.PlaySFX(m_sfx_shoot);
			Debug.LogError("GameOver You Died!");




			//ICS REMOVE COMMENT
			if (PlayerScript.instance.Get_IsPlayerAlive ())
			{
				PlayerScript.instance.Set_IsPlayerAlive(false);
				StartCoroutine(HasPlayerDied());
				OCameraMgr.Instance.CameraShake(.5f,.5f);
			}

		}
		TimerObject.SetActive(true);

		//Debug.Log ("@@@@@@@@@ m_timeToShot " + m_timeToShot);
		if(m_timeToShot >= AgentConfig.AGENT_SHOOT_SPEED - .2f)
		{
			StartCoroutine(DoBlinks(.03f, .1f));
		}
		//StartCoroutine(BlinkCrosshair(3));
		//float lerp = Mathf.PingPong(Time.time, 1) / 1;
		//m_AgentSprite.renderer.material.color = Color.Lerp(Color.white, Color.black, Mathf.InverseLerp(m_maxTimeToShot,0, m_timeToShot));
		TimerObject.gameObject.renderer.material.SetFloat ("_Cutoff", Mathf.InverseLerp(m_maxTimeToShot,0, m_timeToShot));
	}

	IEnumerator HasPlayerDied()
	{

		yield return new WaitForSeconds (1f);
		OGameMgr.Instance.Fader.Fade(Color.red, 0, 1f, 1f, deactivateOnFadeEnd: false, onFadeEnd: OnFadeEndAgentScript);
	}
	
	void OnFadeEndAgentScript()
	{
		OGameMgr.Instance.Fader.Fade(Color.red, 1f, 0, 1f);
		OGameMgr.Instance.ShowPopup("SResultPopup");
	}

	public float Get_DeathTime()
	{
		return m_timeToShot;
	}

	void OnTriggerEnter(Collider other) 
	{
		//Debug.Log("other " + other);
		if(other.tag == "BoarderLeft")
		{
//			Debug.Log("BoarderLeft");
			m_agentDirection = 1;
		}
		if(other.tag == "BoarderRight")
		{
//			Debug.Log("BoarderLeft");
			m_agentDirection = -1;
		}
	}

	IEnumerator BlinkCrosshair(int numberOfTimesToBlink, int blinkCount = 0)
	{
		while(blinkCount < numberOfTimesToBlink)
		{
			Debug.Log("blinkCount " + blinkCount);
			Debug.Log("numberOfTimesToBlink " + numberOfTimesToBlink);
			m_Crosshair.renderer.enabled = !m_Crosshair.renderer.enabled;
			if(m_Crosshair.renderer.enabled = true)
				blinkCount++;

			yield return new WaitForSeconds(1f);
		}
	}

	IEnumerator DoBlinks(float duration, float blinkTime) {
		while (duration > 0f) {
			duration -= Time.deltaTime;
			//Debug.Log("@@@@@@ " + duration);
			//toggle renderer
			m_Crosshair.renderer.enabled = !m_Crosshair.renderer.enabled;
			
			//wait for a bit
			yield return new WaitForSeconds(blinkTime);
		}
		
		//make sure renderer is enabled when we exit
		m_Crosshair.renderer.enabled = true;
	}

	public void KilledTheAgent()
	{

		for (int i = 0; i < this.transform.GetChildCount(); ++i)
		{
			this.transform.GetChild(i).gameObject.renderer.enabled = false;
		}
		m_Crosshair.gameObject.renderer.enabled = false;

		m_agentSpeed = 0f;

		StartCoroutine(AgentStartToDestroy());

		this.gameObject.collider.enabled = false;

		m_lblScore.gameObject.SetActive (true);
		m_lblScore.gameObject.renderer.enabled = true;

		//Debug.Log("@@@@@@@@@@@ KILLED THE AGENT");

		return;
		if(Time.fixedTime%.5<.2)
		{

		}
		else
		{
			for (int i = 0; i < this.transform.GetChildCount(); ++i)
			{
				this.transform.GetChild(i).gameObject.renderer.enabled = true;
			}
		}


	}
	public void SetAgentScore(int nScore)
	{
		m_lblScore.UpdateLabel ("+" + nScore);
		//m_Crosshair.renderer.material.color.a = Mathf.Lerp(renderer.material.color.a, 0, Time.deltaTime * 2);
	}

	public void Set_IsAgentCanShoot(bool isCanShoot)
	{
		m_isAgentCanShoot = isCanShoot;
	}
	public void Set_IsAgentAlive(bool isAlive)
	{
		Anim.SetBool ("isAgentAlive", isAlive);
		AnimFace.SetBool ("isAgentAlive", isAlive);
		m_isAgentAlive = isAlive;
	}

	public bool Get_IsAgentAlive()
	{
		return m_isAgentAlive;
	}
	public bool Get_IsAgentCanShoot()
	{
		return m_isAgentCanShoot;
	}

	IEnumerator AgentStartToDestroy()
	{
		yield return new WaitForSeconds (1f);
		Destroy (this.gameObject);
	}

	public void ResetAgentValues()
	{
		m_isAgentAlive = true;
		m_isAgentCanShoot = false;
	
		m_AgentShootOnce = false;
		tempCallOnce = true;	

		m_AgentSprite.renderer.material.color = Color.white;

		m_agentDirection = -1;
		TimerObject.SetActive(false);
		//TimerObject.gameObject.renderer.material.SetFloat ("_Cutoff", 0.005f);
		TimerObject.gameObject.renderer.material.SetFloat ("_Cutoff", OTuneMgr.Instance.GetFloat("AlphaCutoff"));

		m_timeToWalk = Random.Range (AgentConfig.AGENT_WALK_TIME, AgentConfig.AGENT_MAX_WALK_TIME);

		m_timeToShot = AgentConfig.AGENT_SHOOT_SPEED;
		m_maxTimeToShot = AgentConfig.AGENT_SHOOT_SPEED;
		m_agentSpeed = Random.Range (AgentConfig.AGENT_SPEED, AgentConfig.AGENT_MAX_SPEED);

		Anim.SetBool ("isAgentAlive", true);
		Anim.SetTrigger ("AgentWalkAnim");

		AnimFace.SetBool ("isAgentAlive", true);
		AnimFace.SetTrigger ("AgentWalkAnim");

		m_lblScore.gameObject.SetActive (false);

		for (int i = 0; i < this.transform.GetChildCount(); ++i)
		{
			this.transform.GetChild(i).gameObject.renderer.enabled = true;
		}
		m_Crosshair.gameObject.renderer.enabled = true;
	}


}

﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {

	public static PlayerScript instance;

	public int m_playerBullets;
	public bool m_isOutOfBullets = false;
	
	public int m_nRound = 0;

	private int m_playerScore = 0;
	private bool m_isPlayerAlive;

	//labels
	public OLabel m_lblBullet;
	public OLabel m_lblScore;

	// Use this for initialization
	void Start () {
		instance = this;

		m_isPlayerAlive = true;
		m_playerBullets = AgentConfig.PLAYER_MAX_BULLET;
	}
	
	// Update is called once per frame
	void Update () {
		m_lblBullet.UpdateLabel ("Bullets: "  + m_playerBullets);
		m_lblScore.UpdateLabel ("Score: "  + Get_PlayerScore());
	
		if(m_playerBullets <= 0)
			m_isOutOfBullets = true;

		if(Input.GetKeyDown(KeyCode.T))
		{
			OGameMgr.Instance.ShowPopup("SRoundPopup");
		}
	}

	public bool Get_IsOutOfBullets()
	{	return m_isOutOfBullets;
	}

	public void Set_PlayerBullets(int nRemainingBullets)
	{	m_playerBullets = nRemainingBullets;
	}

	public int Get_PlayerBullets()
	{	return m_playerBullets;
	}

	public void Set_PlayerScore(int nCurrentScore)
	{	m_playerScore += nCurrentScore;
	}
	
	public int Get_PlayerScore()
	{	return m_playerScore;
	}

	public void Set_IsPlayerAlive(bool isAlive)
	{	m_isPlayerAlive = isAlive;
	}

	public bool Get_IsPlayerAlive()
	{	return m_isPlayerAlive;
	}

	public void ResetPlayerValues()
	{
		m_nRound = 0;
		m_playerScore = 0;
		m_isPlayerAlive = true;
	}
}
